package br.com.projetox.projetomenu.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by matheus on 7/21/15.
 */
public class ProductType implements Serializable{

    @Expose
    private Long id;
    @SerializedName("order_by")
    @Expose
    private Long orderBy;
    @Expose
    private String name;
    @Expose
    private String description;
    @SerializedName("category_id")
    @Expose
    private Long categoryId;

    public ProductType (){
        this.name       = "";
        this.categoryId = 0l;
    }

    public ProductType(String name, Long categoryId) {
        this.name = name;
        this.categoryId = categoryId;
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The orderBy
     */
    public Long getOrderBy() {
        return orderBy;
    }

    /**
     *
     * @param orderBy
     * The order_by
     */
    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The category
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     *
     * @param categoryId
     * The category_id
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

}