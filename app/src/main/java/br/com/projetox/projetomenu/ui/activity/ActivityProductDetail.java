package br.com.projetox.projetomenu.ui.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Product;
import br.com.projetox.projetomenu.ui.activity.base.ActivityBase;
import br.com.projetox.projetomenu.utils.Utils;

@EActivity(R.layout.acivity_product_detail)
public class ActivityProductDetail extends ActivityBase {

    @Extra("PRODUCT_DETAIL")
    @InstanceState
    public Product product;

    @ViewById(R.id.activity_product_detail_image_view)
    public ImageView productImage;

    @ViewById(R.id.activity_product_detail_image_progress)
    public View productImageProgress;

    @ViewById(R.id.activity_product_detail_name_text)
    public TextView productName;

    @ViewById(R.id.activity_product_detail_price_text)
    public TextView productPrice;

    @ViewById(R.id.activity_product_detail_description_text)
    public TextView productDescription;

    @ViewById(R.id.activity_product_detail_ingredient_text)
    public TextView productIngredient;

    @ViewById(R.id.activity_product_detail_description_view)
    public View productDescriptionView;

    @ViewById(R.id.activity_product_detail_ingredient_view)
    public View productIngredientView;

    @Override
    @SuppressWarnings("ConstantConditions")
    @AfterViews
    public void setupToobar() {
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @AfterViews
    public void updateUI(){
        productName.setText(product.getName().toUpperCase());
        productPrice.setText(Utils.getPriceLabel(this, product.getPrice(), null));
        productDescription.setText(product.getDescription());
        productIngredient.setText(product.getIngredient());
        productDescriptionView.setVisibility(product.getDescription() == null || product.getDescription().isEmpty() ? View.GONE : View.VISIBLE);
        productIngredientView.setVisibility(product.getIngredient() == null || product.getIngredient().isEmpty()? View.GONE: View.VISIBLE);
        Utils.getImage(this, productImageProgress, productImage, product.getImgUrl());
    }
}
