package br.com.projetox.projetomenu.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Category;
import de.greenrobot.event.EventBus;

/**
 * Created by matheus on 7/21/15.
 */
public class MenuRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Category> categoryList;

    public MenuRecyclerAdapter(Context context, List<Category> categoryList) {
        this.mContext      = context;
        this.categoryList = categoryList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_menu_list_item, parent, false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        setupCategory((CategoryViewHolder) viewHolder, position);
    }

    private void setupCategory(final CategoryViewHolder viewHolder, int position) {
        final Category category = categoryList.get(position);
        viewHolder.categoryText.setText(category.getName().toUpperCase());
        viewHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!category.isSelected())
                    EventBus.getDefault().post(category);
            }
        });

        if(category.isSelected()){
            viewHolder.categoryText.setTextColor(mContext.getResources().getColor(R.color.white));
            if (android.os.Build.VERSION.SDK_INT >= 21){
                viewHolder.itemLayout.setBackground(mContext.getDrawable(R.drawable.selected_category_selector));
            } else{
                viewHolder.itemLayout.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.selected_category_selector));
            }
        }else{
            viewHolder.categoryText.setTextColor(mContext.getResources().getColor(R.color.accent_color_tree));
            if (android.os.Build.VERSION.SDK_INT >= 21){
                viewHolder.itemLayout.setBackground(mContext.getDrawable(R.drawable.category_selector));
            } else{
                viewHolder.itemLayout.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.category_selector));
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        public View     itemLayout;
        public TextView categoryText;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            this.itemLayout   = itemView.findViewById(R.id.fragment_menu_item_layout);
            this.categoryText = (TextView) itemView.findViewById(R.id.fragment_menu_item_text);
        }
    }
}