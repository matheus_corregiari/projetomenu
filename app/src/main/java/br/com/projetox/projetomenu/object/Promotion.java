package br.com.projetox.projetomenu.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by matheus on 7/21/15.
 */
public class Promotion implements Serializable{

    @Expose
    private Long id;
    @SerializedName("order_by")
    @Expose
    private Long orderBy;
    @Expose
    private String name;
    @Expose
    private String description;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("price_after")
    @Expose
    private Double priceAfter;
    @SerializedName("price_before")
    @Expose
    private Double priceBefore;

    public Promotion() {
        this.id          = 0l;
        this.orderBy     = 0l;
        this.name        = "";
        this.description = "";
        this.imgUrl      = "";
        this.priceAfter  = 0d;
        this.priceBefore = 0d;
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The orderBy
     */
    public Long getOrderBy() {
        return orderBy;
    }

    /**
     *
     * @param orderBy
     * The order_by
     */
    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     *
     * @param imgUrl
     * The img_url
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     *
     * @return
     * The priceAfter
     */
    public Double getPriceAfter() {
        return priceAfter;
    }

    /**
     *
     * @param priceAfter
     * The priceAfter
     */
    public void setPriceAfter(Double priceAfter) {
        this.priceAfter = priceAfter;
    }

    /**
     *
     * @return
     * The priceBefore
     */
    public Double getPriceBefore() {
        return priceBefore;
    }

    /**
     *
     * @param priceBefore
     * The priceBefore
     */
    public void setPriceBefore(Double priceBefore) {
        this.priceBefore = priceBefore;
    }

}