package br.com.projetox.projetomenu.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by matheus on 7/21/15.
 */
public class Product implements Serializable{

    @Expose
    private Long id;
    @SerializedName("order_by")
    @Expose
    private Long orderBy;
    @SerializedName("category_id")
    @Expose
    private Long categoryId;
    @SerializedName("product_type_id")
    @Expose
    private Long productTypeId;
    @Expose
    private Boolean active;
    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private String ingredient;
    @Expose
    private Double price;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;

    public Product() {
        this.id            = 0l;
        this.orderBy       = 0l;
        this.categoryId    = 0l;
        this.productTypeId = 0l;
        this.active        = true;
        this.name          = "";
        this.description   = "";
        this.ingredient    = "";
        this.price         = 0d;
        this.imgUrl        = "";
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The orderBy
     */
    public Long getOrderBy() {
        return orderBy;
    }

    /**
     *
     * @param orderBy
     * The order_by
     */
    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    /**
     *
     * @return
     * The category
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     *
     * @param categoryId
     * The category_id
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    /**
     *
     * @return
     * The productTypeId
     */
    public Long getProductTypeId() {
        return productTypeId;
    }

    /**
     *
     * @param productTypeId
     * The productTypeId
     */
    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    /**
     *
     * @return
     * The active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * @param active
     * The active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The ingredient
     */
    public String getIngredient() {
        return ingredient;
    }

    /**
     *
     * @param ingredient
     * The ingredient
     */
    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    /**
     *
     * @return
     * The price
     */
    public Double getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     *
     * @return
     * The imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     *
     * @param imgUrl
     * The img_url
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

}