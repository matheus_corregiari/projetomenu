package br.com.projetox.projetomenu;

import android.content.Context;

import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.object.CategoryType;

/**
 * Created by matheus on 8/3/15.
 */
public class Constants {

    public static Category aboutCategory(Context context){
        Category category = new Category();
        category.setId(0l);
        category.setOrderBy(0l);
        category.setName(context.getString(R.string.category_about_name));
        category.setDescription(context.getString(R.string.category_about_description));
        category.setCategoryType(new CategoryType(context.getString(R.string.category_about_type_name)));
        return category;
    }

}
