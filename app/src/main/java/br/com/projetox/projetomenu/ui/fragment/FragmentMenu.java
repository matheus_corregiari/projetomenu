package br.com.projetox.projetomenu.ui.fragment;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.projetox.projetomenu.BuildConfig;
import br.com.projetox.projetomenu.Constants;
import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.api.API;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.ui.adapter.MenuRecyclerAdapter;
import br.com.projetox.projetomenu.ui.fragment.base.FragmentBase;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EFragment(R.layout.fragment_menu)
public class FragmentMenu extends FragmentBase {

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;
    public boolean mFromSavedInstanceState;

    @InstanceState
    public int selectecPosition = 0;

    @ViewById(R.id.fragment_menu_recycler_view)
    public RecyclerView   recyclerView;
    @ViewById(R.id.fragment_menu_sobre)
    public View sobreView;

    public List<Category> categoryList;

    public FragmentMenu() {
    }

    @Override
    @AfterViews
    @Background
    public void onServerRequest(){
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(BuildConfig.HOST).build();
        API api = restAdapter.create(API.class);
        showProgressView();
        api.getCategories(BuildConfig.REQUEST_APPLICATION_ID, new Callback<List<Category>>() {
            @Override
            public void success(List<Category> categories, Response response) {
                if (response.getStatus() == 200 && categories != null) {
                    if (!categories.isEmpty()) {
                        categoryList = categories;
                        saveLocal("FragmentMenu:" + BuildConfig.REQUEST_APPLICATION_ID, categoryList);
                    } else {
                        showEmptyView();
                    }
                } else {
                    if (categoryList == null || categoryList.isEmpty()) {
                        showErrorView();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                categoryList = getLocal("FragmentMenu:"+BuildConfig.REQUEST_APPLICATION_ID);
                if (categoryList != null && !categoryList.isEmpty()) {
                    configuraRecyclerView();
                    EventBus.getDefault().post(selectecPosition >= 0 ? categoryList.get(selectecPosition) : Constants.aboutCategory(getActivity()));
                    showSuccessView();
                }else{
                    showErrorView();
                    EventBus.getDefault().post(Constants.aboutCategory(getActivity()));
                }
            }
        });
    }

    public void saveLocal(String key, List<Category> categories){
        if(key == null || key.isEmpty()){
            return;
        }
        if(categories == null || categories.isEmpty()){
            return;
        }
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "CATEGORIES");
            snappydb.put(key, categories.toArray());
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        configuraRecyclerView();
        EventBus.getDefault().post(selectecPosition >= 0 ? categoryList.get(selectecPosition) : Constants.aboutCategory(getActivity()));
        showSuccessView();
    }

    public List<Category> getLocal(String key){
        if(key == null || key.isEmpty()){
            return null;
        }
        List<Category> categories = new ArrayList<>();
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "CATEGORIES");
            Collections.addAll(categories, snappydb.getArray(key, Category.class));
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        return categories;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hasOptionsMenu();
        if(savedInstanceState != null){
            this.mFromSavedInstanceState = true;
        }
    }

    @Click(R.id.fragment_menu_sobre)
    public void onAboutClick(View v){
        EventBus.getDefault().post(Constants.aboutCategory(getActivity()));
    }

    @Click(R.id.image_menu)
    public void onLogoClick(View v){
        //do nothing
    }

    @Override
    @UiThread
    public void configuraRecyclerView(){
        if(selectecPosition >= 0){
            categoryList.get(selectecPosition).setSelected(true);
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);
        MenuRecyclerAdapter adapter = new MenuRecyclerAdapter(getActivity(), categoryList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    public void setUp(DrawerLayout drawerLayout, Toolbar toolbar) {
        this.drawerLayout = drawerLayout;
        this.actionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }
        };

        this.drawerLayout.setDrawerListener(actionBarDrawerToggle);
        this.drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onEvent(Category category){
        if(!isTablet){
            drawerLayout.closeDrawers();
        }
        this.selectecPosition = category.getId().intValue();
        for(int i = 0; i < categoryList.size(); i++){
            Category item = categoryList.get(i);
            if(item.getId().longValue() == category.getId().longValue()){
                selectecPosition = i;
            }else{
                item.setSelected(false);
            }
        }

        if(category.getId() == 0){
            selectecPosition = -1;
        }
        configuraRecyclerView();
    }
}