package br.com.projetox.projetomenu.ui.adapter;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.ui.fragment.FragmentProductList;
import br.com.projetox.projetomenu.ui.fragment.FragmentSubCategoryList;
import br.com.projetox.projetomenu.utils.Utils;

/**
 * Created by matheus on 7/21/15.
 */
public class SubCategoryRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Category> categoryList;

    public SubCategoryRecyclerAdapter(Context context, List<Category> categoryList) {
        this.mContext       = context;
        this.categoryList   = categoryList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch(viewType){
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_subcategory_list_item, parent, false);
                return new CategoryViewHolder(itemView);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if(getItemViewType(position) == 1) {
            setupCategory((CategoryViewHolder) viewHolder, position);
        }
    }

    private void setupCategory(final CategoryViewHolder viewHolder, int position) {
        final Category category = categoryList.get(position);
        viewHolder.nameText.setText(category.getName());
        Utils.getImage(mContext, viewHolder.imageProgress, viewHolder.imageView, category.getImgUrl());
        viewHolder.layoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category.getCategoryIds() == null || category.getCategoryIds().isEmpty()) {
                    ((ActionBarActivity) mContext).getSupportFragmentManager().beginTransaction().replace(R.id.content, FragmentProductList.newInstance(category, true)).addToBackStack("").commit();
                } else {
                    ((ActionBarActivity) mContext).getSupportFragmentManager().beginTransaction().replace(R.id.content, FragmentSubCategoryList.newInstance(category, true)).addToBackStack("").commit();
                }
            }
        });
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        public View      layoutView;
        public ImageView imageView;
        public View      imageProgress;
        public TextView  nameText;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            this.layoutView    =  itemView.findViewById(R.id.fragment_subcategory_list_layout_view);
            this.imageView     = (ImageView) itemView.findViewById(R.id.fragment_subcategory_list_image_view);
            this.imageProgress =  itemView.findViewById(R.id.fragment_subcategory_list_image_progress);
            this.nameText      = (TextView)  itemView.findViewById(R.id.fragment_subcategory_list_name_text);
        }
    }

}