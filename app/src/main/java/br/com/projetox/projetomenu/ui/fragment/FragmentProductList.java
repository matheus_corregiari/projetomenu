package br.com.projetox.projetomenu.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crashlytics.android.Crashlytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.projetox.projetomenu.BuildConfig;
import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.api.API;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.object.Product;
import br.com.projetox.projetomenu.object.ProductType;
import br.com.projetox.projetomenu.ui.activity.ActivityMain;
import br.com.projetox.projetomenu.ui.adapter.ProductRecyclerAdapter;
import br.com.projetox.projetomenu.ui.fragment.base.FragmentBase;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ActivityMain}
 * on handsets.
 */
@EFragment(R.layout.fragment_product_list)
public class FragmentProductList extends FragmentBase {

    @ViewById(R.id.fragment_product_list_recycler_view)
    public RecyclerView recyclerView;

    public List<Product> productList;

    public List<ProductType> productTypeList;

    public FragmentProductList() {
    }

    public static Fragment newInstance(Category category, boolean showBackButton){
        Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_KEY_ARGUMENT_CATEGORY_ID, category);
        arguments.putBoolean(BUNDLE_KEY_ARGUMENT_BACK_CATEGORY_ID, showBackButton);
        FragmentProductList_ fragment = new FragmentProductList_();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    @AfterViews
    public void onServerRequest(){
        productTypeList = category.getProductTypes();
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(BuildConfig.HOST).build();
        API api = restAdapter.create(API.class);
        showProgressView();
        String key = "FragmentProductList:"+BuildConfig.REQUEST_APPLICATION_ID+":"+category.getId();
        long[] aux = new long[category.getCategoryIds().size()+1];
        aux[0] = category.getId();
        for(int i = 1; i < category.getCategoryIds().size(); i++){
            key+=":"+category.getCategoryIds().get(i);
            aux[i] = category.getCategoryIds().get(i);
        }
        productList = getLocal(key);
        if (productList != null && !productList.isEmpty()) {
            configuraRecyclerView();
            showSuccessView();
        }
        final String finalKey = key;
        api.getProducts(BuildConfig.REQUEST_APPLICATION_ID, aux, new Callback<List<Product>>() {
            @Override
            public void success(List<Product> products, Response response) {
                if (response.getStatus() == 200 && products != null) {
                    if (!products.isEmpty()) {
                        productList = products;
                        saveLocal(finalKey, productList);
                    } else {
                        showEmptyView();
                    }
                } else {
                    if (productList == null || productList.isEmpty()) {
                        showErrorView();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (productList == null || productList.isEmpty()) {
                    showErrorView();
                }
            }
        });
    }

    @Background
    public void saveLocal(String key, List<Product> products){
        if(key == null || key.isEmpty()){
            return;
        }
        if(products == null || products.isEmpty()){
            return;
        }
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "PRODUCTS");
            snappydb.put(key, products.toArray());
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        configuraRecyclerView();
        showSuccessView();
    }

    public List<Product> getLocal(String key){
        if(key == null || key.isEmpty()){
            return null;
        }
        List<Product> products = new ArrayList<>();
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "PRODUCTS");
            Collections.addAll(products, snappydb.getArray(key, Product.class));
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        return products;
    }

    @Override
    @UiThread
    public void configuraRecyclerView(){
        final ProductRecyclerAdapter adapter = new ProductRecyclerAdapter(getActivity(), productList, productTypeList);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), isTablet? (landscape? 3 : 2) : (landscape? 2 : 1));
        layoutManager.scrollToPosition(0);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(!isTablet && !landscape){
                    return 1;
                }
                if(!isTablet && landscape){
                    return adapter.getItemViewType(position) == 0? 1 : 2;
                }
                return adapter.getItemViewType(position) == 0? 1 : (landscape? 3 : 2);
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
