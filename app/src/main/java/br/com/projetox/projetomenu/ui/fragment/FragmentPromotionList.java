package br.com.projetox.projetomenu.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crashlytics.android.Crashlytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.projetox.projetomenu.BuildConfig;
import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.api.API;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.object.Promotion;
import br.com.projetox.projetomenu.ui.activity.ActivityMain;
import br.com.projetox.projetomenu.ui.adapter.PromotionRecyclerAdapter;
import br.com.projetox.projetomenu.ui.fragment.base.FragmentBase;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ActivityMain}
 * on handsets.
 */
@EFragment(R.layout.fragment_promotion_list)
public class FragmentPromotionList extends FragmentBase {

    @ViewById(R.id.fragment_promotion_list_recycler_view)
    public RecyclerView recyclerView;

    public List<Promotion> promotionList;

    public FragmentPromotionList() {
    }

    public static Fragment newInstance(Category category, boolean showBackButton){
        Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_KEY_ARGUMENT_CATEGORY_ID, category);
        arguments.putBoolean(BUNDLE_KEY_ARGUMENT_BACK_CATEGORY_ID, showBackButton);
        FragmentPromotionList_ fragment = new FragmentPromotionList_();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    @AfterViews
    @Background
    public void onServerRequest(){
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(BuildConfig.HOST).build();
        API api = restAdapter.create(API.class);
        showProgressView();
        String key = "FragmentPromotionsList:"+BuildConfig.REQUEST_APPLICATION_ID+":"+category.getId();
        promotionList = getLocal(key);
        if (promotionList != null && !promotionList.isEmpty()) {
            configuraRecyclerView();
            showSuccessView();
        }
        final String finalKey = key;
        api.getPromotions(BuildConfig.REQUEST_APPLICATION_ID, category.getId(), new Callback<List<Promotion>>() {
            @Override
            public void success(List<Promotion> products, Response response) {
                if (response.getStatus() == 200 && products != null) {
                    if (!products.isEmpty()) {
                        promotionList = products;
                        saveLocal(finalKey, promotionList);
                    } else {
                        showEmptyView();
                    }
                } else {
                    if (promotionList == null || promotionList.isEmpty()) {
                        showErrorView();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (promotionList == null || promotionList.isEmpty()) {
                    showErrorView();
                }
            }
        });
    }

    @Background
    public void saveLocal(String key, List<Promotion> promotions){
        if(key == null || key.isEmpty()){
            return;
        }
        if(promotions == null || promotions.isEmpty()){
            return;
        }
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "PROMOTIONS");
            snappydb.put(key, promotions.toArray());
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        configuraRecyclerView();
        showSuccessView();
    }

    public List<Promotion> getLocal(String key){
        if(key == null || key.isEmpty()){
            return null;
        }
        List<Promotion> promotions = new ArrayList<>();
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "PROMOTIONS");
            Collections.addAll(promotions, snappydb.getArray(key, Promotion.class));
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        return promotions;
    }

    @Override
    @UiThread
    public void configuraRecyclerView(){
        final PromotionRecyclerAdapter adapter = new PromotionRecyclerAdapter(getActivity(), promotionList);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), (landscape? 2 : 1));
        layoutManager.scrollToPosition(0);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(!isTablet){
                    return landscape? 1 : 1;
                }
                return adapter.getItemViewType(position) == 1? 1 : (landscape? 2 : 1);
            }
        });

        layoutManager.scrollToPosition(0);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
