package br.com.projetox.projetomenu.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matheus on 7/21/15.
 */
public class Category implements Serializable{

    @Expose
    private Long id;
    @SerializedName("order_by")
    @Expose
    private Long orderBy;
    @Expose
    private String name;
    @SerializedName("category_type")
    @Expose
    private CategoryType categoryType;
    @Expose
    private String description;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("category_ids")
    @Expose
    private List<Long> categoryIds = new ArrayList<Long>();
    @SerializedName("product_type")
    @Expose
    private List<ProductType> productTypes = new ArrayList<ProductType>();

    private boolean selected;

    public Category() {
        this.id           = 0l;
        this.orderBy      = 0l;
        this.name         = "";
        this.categoryType = null;
        this.description  = "";
        this.imgUrl       = "";
        this.categoryIds  = new ArrayList<>();
        this.productTypes = new ArrayList<>();
        this.selected     = false;
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    public Category withId(Long id) {
        this.id = id;
        return this;
    }

    /**
     *
     * @return
     * The orderBy
     */
    public Long getOrderBy() {
        return orderBy;
    }

    /**
     *
     * @param orderBy
     * The order_by
     */
    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    public Category withOrderBy(Long orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public Category withName(String name) {
        this.name = name;
        return this;
    }

    /**
     *
     * @return
     * The categoryType
     */
    public CategoryType getCategoryType() {
        return categoryType;
    }

    /**
     *
     * @param categoryType
     * The category_type
     */
    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public Category withCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
        return this;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public Category withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     *
     * @return
     * The imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     *
     * @param imgUrl
     * The img_url
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     *
     * @return
     * The categoryIds
     */
    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    /**
     *
     * @param categoryIds
     * The categoryIds
     */
    public void setCategoryIds(List<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public Category withCategory(List<Long> category) {
        this.categoryIds = category;
        return this;
    }

    /**
     *
     * @return
     * The productTypes
     */
    public List<ProductType> getProductTypes() {
        return productTypes;
    }

    /**
     *
     * @param productTypes
     * The productTypes
     */
    public void setProductTypes(List<ProductType> productTypes) {
        this.productTypes = productTypes;
    }

    public Category withProductType(List<ProductType> productTypes) {
        this.productTypes = productTypes;
        return this;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}