package br.com.projetox.projetomenu.api;

import java.util.List;

import br.com.projetox.projetomenu.object.Application;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.object.Product;
import br.com.projetox.projetomenu.object.Promotion;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by matheus on 7/31/15.
 */
public interface API {

    @GET("/categories.json")
    public void getCategories(@Query("application_id") long applicationId, Callback<List<Category>> response);

    @GET("/categories.json")
    public void getSubCategories(@Query("application_id") long applicationId, @Query("category_id[]") long[] categoryIds, Callback<List<Category>> response);

    @GET("/products.json")
    public void getProducts(@Query("application_id") long applicationId, @Query("category_id[]") long[] categoryIds, Callback<List<Product>> response);

    @GET("/promotions.json")
    public void getPromotions(@Query("application_id") long applicationId, @Query("category_id") long categoryId, Callback<List<Promotion>> response);

    @GET("/applications/{applicationId}.json")
    public void getApplication(@Path("applicationId") long applicationId, Callback<Application> response);

}
