package br.com.projetox.projetomenu.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crashlytics.android.Crashlytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.projetox.projetomenu.BuildConfig;
import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.api.API;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.ui.activity.ActivityMain;
import br.com.projetox.projetomenu.ui.adapter.SubCategoryRecyclerAdapter;
import br.com.projetox.projetomenu.ui.fragment.base.FragmentBase;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ActivityMain}
 * on handsets.
 */
@EFragment(R.layout.fragment_subcategory_list)
public class FragmentSubCategoryList extends FragmentBase {

    @ViewById(R.id.fragment_subcategory_list_recycler_view)
    public RecyclerView recyclerView;

    public List<Category> categoryList;

    public FragmentSubCategoryList() {
    }

    public static Fragment newInstance(Category category, boolean showBackButton){
        Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_KEY_ARGUMENT_CATEGORY_ID, category);
        arguments.putBoolean(BUNDLE_KEY_ARGUMENT_BACK_CATEGORY_ID, showBackButton);
        FragmentSubCategoryList_ fragment = new FragmentSubCategoryList_();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    @AfterViews
    @Background
    public void onServerRequest(){
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(BuildConfig.HOST).build();
        API api = restAdapter.create(API.class);
        showProgressView();
        String key = "FragmentSubCategoryList:"+BuildConfig.REQUEST_APPLICATION_ID+":"+category.getId();
        long[] aux = new long[category.getCategoryIds().size()];
        for(int i = 0; i < category.getCategoryIds().size(); i++){
            key+=":"+category.getCategoryIds().get(i);
            aux[i] = category.getCategoryIds().get(i);
        }
        categoryList = getLocal(key);
        if (categoryList != null && !categoryList.isEmpty()) {
            configuraRecyclerView();
            showSuccessView();
        }
        final String finalKey = key;
        api.getSubCategories(BuildConfig.REQUEST_APPLICATION_ID, aux, new Callback<List<Category>>() {
            @Override
            public void success(List<Category> categories, Response response) {
                if (response.getStatus() == 200 && categories != null) {
                    if (!categories.isEmpty()) {
                        categoryList = categories;
                        saveLocal(finalKey, categoryList);
                    } else {
                        showEmptyView();
                    }
                } else {
                    if (categoryList == null || categoryList.isEmpty()) {
                        showErrorView();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (categoryList == null || categoryList.isEmpty()) {
                    showErrorView();
                }
            }
        });
    }

    @Background
    public void saveLocal(String key, List<Category> categories){
        if(key == null || key.isEmpty()){
            return;
        }
        if(categories == null || categories.isEmpty()){
            return;
        }
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "SUBCATEGORY");
            snappydb.put(key, categories.toArray());
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        configuraRecyclerView();
        showSuccessView();
    }

    public List<Category> getLocal(String key){
        if(key == null || key.isEmpty()){
            return null;
        }
        List<Category> categories = new ArrayList<>();
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "SUBCATEGORY");
            Collections.addAll(categories, snappydb.getArray(key, Category.class));
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        return categories;
    }

    @Override
    @UiThread
    public void configuraRecyclerView(){
        final SubCategoryRecyclerAdapter adapter = new SubCategoryRecyclerAdapter(getActivity(), categoryList);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), isTablet? (landscape? 3 : 2) : (landscape? 2 : 1));
        layoutManager.scrollToPosition(0);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(!isTablet && !landscape){
                    return 1;
                }
                if(!isTablet && landscape){
                    return adapter.getItemViewType(position) == 1? 1 : 2;
                }
                return adapter.getItemViewType(position) == 1? 1 : (landscape? 3 : 2);
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
