package br.com.projetox.projetomenu.ui.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.ui.activity.base.ActivityBase;
import br.com.projetox.projetomenu.ui.fragment.FragmentAbout;
import br.com.projetox.projetomenu.ui.fragment.FragmentMenu_;
import br.com.projetox.projetomenu.ui.fragment.FragmentProductList;
import br.com.projetox.projetomenu.ui.fragment.FragmentPromotionList;
import br.com.projetox.projetomenu.ui.fragment.FragmentSubCategoryList;
import de.greenrobot.event.EventBus;

@EActivity(R.layout.acivity_main)
public class ActivityMain extends ActivityBase {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!getResources().getBoolean(R.bool.isTablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @AfterViews
    public void setupMenuButton(){
        if(!isTablet){
            FragmentMenu_ menuFragment = (FragmentMenu_)
                    getSupportFragmentManager().findFragmentById(R.id.drawer);
            DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            menuFragment.setUp(drawerLayout, this.toolbar);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @SuppressWarnings("ConstantConditions")
    public void onEvent(Category category){
        if(category == null){
            return;
        }
        if(!isTablet){
            getSupportActionBar().setTitle(category.getName());
        }
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if(category.getCategoryType().getName().equals("PRODUCT")){
            if(category.getCategoryIds() == null || category.getCategoryIds().isEmpty()){
                getSupportFragmentManager().beginTransaction().replace(R.id.content, FragmentProductList.newInstance(category, false)).commit();
            }else{
                getSupportFragmentManager().beginTransaction().replace(R.id.content, FragmentSubCategoryList.newInstance(category, false)).commit();
            }
        }
        if(category.getCategoryType().getName().equals("PROMOTION")){
            getSupportFragmentManager().beginTransaction().replace(R.id.content, FragmentPromotionList.newInstance(category, false)).commit();
        }
        if(category.getCategoryType().getName().equals("ABOUT")){
            getSupportFragmentManager().beginTransaction().replace(R.id.content, FragmentAbout.newInstance(category)).commit();
        }
    }

}
