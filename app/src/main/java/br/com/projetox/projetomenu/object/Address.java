package br.com.projetox.projetomenu.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matheus on 7/21/15.
 */
public class Address implements Serializable{

    @Expose
    private Long id;
    @SerializedName("application_id")
    @Expose
    private Long applicationId;
    @SerializedName("order_by")
    @Expose
    private Long orderBy;
    @Expose
    private String street;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @Expose
    private String neighborhood;
    @Expose
    private String city;
    @Expose
    private String state;
    @Expose
    private Double latitude;
    @Expose
    private Double longitude;
    @Expose
    private Boolean active;
    @Expose
    private List<Phone> phones = new ArrayList<Phone>();

    public Address() {
        this.id            = 0l;
        this.applicationId = 0l;
        this.orderBy       = 0l;
        this.street        = "";
        this.postalCode    = "";
        this.neighborhood  = "";
        this.city          = "";
        this.state         = "";
        this.latitude      = 0d;
        this.longitude     = 0d;
        this.active        = true;
        this.phones        = new ArrayList<>();
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The applicationId
     */
    public Long getApplicationId() {
        return applicationId;
    }

    /**
     *
     * @param applicationId
     * The application_id
     */
    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    /**
     *
     * @return
     * The orderBy
     */
    public Long getOrderBy() {
        return orderBy;
    }

    /**
     *
     * @param orderBy
     * The order_by
     */
    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    /**
     *
     * @return
     * The street
     */
    public String getStreet() {
        return street;
    }

    /**
     *
     * @param street
     * The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     *
     * @return
     * The postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     * The postal_code
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     *
     * @return
     * The neighborhood
     */
    public String getNeighborhood() {
        return neighborhood;
    }

    /**
     *
     * @param neighborhood
     * The neighborhood
     */
    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * @param active
     * The active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     *
     * @return
     * The phones
     */
    public List<Phone> getPhones() {
        return phones;
    }

    /**
     *
     * @param phones
     * The phones
     */
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

}