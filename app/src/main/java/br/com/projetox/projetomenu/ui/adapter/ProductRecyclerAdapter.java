package br.com.projetox.projetomenu.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Product;
import br.com.projetox.projetomenu.object.ProductType;
import br.com.projetox.projetomenu.ui.activity.ActivityProductDetail_;
import br.com.projetox.projetomenu.utils.Utils;

/**
 * Created by matheus on 7/21/15.
 */
public class ProductRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Object> list;

    public ProductRecyclerAdapter(Context context, List<Product> productList, List<ProductType> productTypeList) {
        this.mContext       = context;
        this.list           = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        List<ProductType> productTypes = new ArrayList<>();
        products.addAll(productList);
        productTypes.addAll(productTypeList);
        if(productTypes.size() > 1){
            boolean showHeader;
            productTypes.add(new ProductType("Outros", null));
            for (Iterator<ProductType> iterator = productTypes.iterator(); iterator.hasNext();) {
                ProductType productType = iterator.next();
                showHeader = false;
                if(!list.contains(productType)){
                    list.add(productType);
                    iterator.remove();
                }
                for (Iterator<Product> iterator2 = products.iterator(); iterator2.hasNext();) {
                    Product product = iterator2.next();
                    if(!list.contains(product) &&
                                ((product.getProductTypeId() == null && productType.getId() == null)
                                        || (product.getProductTypeId() != null && product.getProductTypeId().equals(productType.getId())))){
                        list.add(product);
                        iterator2.remove();
                        showHeader = true;
                    }
                }
                if(!showHeader){
                    list.remove(productType);
                }
            }
        }else{
            list.addAll(products);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch(viewType){
            case 0:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_product_list_item, parent, false);
                return new ProductViewHolder(itemView);
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_product_type_list_item, parent, false);
                return new ProductTypeViewHolder(itemView);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) instanceof Product? 0 : 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch(getItemViewType(position)){
            case 0:
                setupProduct((ProductViewHolder) viewHolder, position);
                break;
            case 1:
                setupProductType((ProductTypeViewHolder) viewHolder, position);
                break;
        }
    }

    private void setupProduct(final ProductViewHolder viewHolder, int position) {
        final Product product = (Product) list.get(position);
        viewHolder.nameText.setText(product.getName());
        viewHolder.priceText.setText(Utils.getPriceLabel(mContext, product.getPrice(), null));
        Utils.getImage(mContext, viewHolder.imageProgress, viewHolder.imageView, product.getImgUrl());
        viewHolder.layoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityProductDetail_.class);
                intent.putExtra("PRODUCT_DETAIL", product);
                mContext.startActivity(intent);
            }
        });
    }

    private void setupProductType(final ProductTypeViewHolder viewHolder, int position) {
        final ProductType productType = (ProductType) list.get(position);
        viewHolder.nameText.setText(productType.getName());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public View      layoutView;
        public ImageView imageView;
        public View      imageProgress;
        public TextView  nameText;
        public TextView  priceText;

        public ProductViewHolder(View itemView) {
            super(itemView);
            this.layoutView    =  itemView.findViewById(R.id.fragment_product_list_layout_view);
            this.imageView     = (ImageView) itemView.findViewById(R.id.fragment_product_list_image_view);
            this.imageProgress =  itemView.findViewById(R.id.fragment_product_list_image_progress);
            this.nameText      = (TextView)  itemView.findViewById(R.id.fragment_product_list_name_text);
            this.priceText     = (TextView)  itemView.findViewById(R.id.fragment_product_list_price_text);
        }
    }

    public class ProductTypeViewHolder extends RecyclerView.ViewHolder {
        public View      layoutView;
        public TextView  nameText;

        public ProductTypeViewHolder(View itemView) {
            super(itemView);
            this.layoutView =  itemView.findViewById(R.id.fragment_product_type_list_layout_view);
            this.nameText   = (TextView)  itemView.findViewById(R.id.fragment_product_type_list_name_text);
        }
    }

}