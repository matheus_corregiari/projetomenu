package br.com.projetox.projetomenu.ui.fragment.base;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.BooleanRes;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.ui.activity.ActivityMain;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ActivityMain}
 * on handsets.
 */
@EFragment
public abstract class FragmentBase extends Fragment {

    public static final String BUNDLE_KEY_ARGUMENT_CATEGORY_ID = "CATEGORY";
    public static final String BUNDLE_KEY_ARGUMENT_BACK_CATEGORY_ID = "BACK_CATEGORY";

    @FragmentArg(BUNDLE_KEY_ARGUMENT_CATEGORY_ID)
    public Category category;
    @FragmentArg(BUNDLE_KEY_ARGUMENT_BACK_CATEGORY_ID)
    public boolean showBackButton = false;

    @BooleanRes(R.bool.isTablet)
    public boolean isTablet;
    @BooleanRes(R.bool.landscape)
    public boolean landscape;

    @ViewById(R.id.success_view)
    public SwipeRefreshLayout successView;
    @ViewById(R.id.empty_view)
    public View emptyView;
    @ViewById(R.id.error_view)
    public View errorView;
    @ViewById(R.id.progress_view)
    public View progressView;

    @ViewById(R.id.fragment_back_view)
    public View backButtonView;
    @ViewById(R.id.fragment_back_button)
    public View backButton;
    @ViewById(R.id.fragment_back_text)
    public TextView backButtonText;

    @AfterViews
    @UiThread
    public void configureRefreshLatout(){
        if(successView != null) {
            successView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onServerRequest();
                }
            });
        }
    }

    @SuppressLint("SetTextI18n")
    @AfterViews
    @UiThread
    public void showBackButtonView(){
        if(isAdded()) {
            if (!showBackButton && getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            if(backButtonView != null) {
                if (isTablet) {
                    backButton.setVisibility(showBackButton ? View.VISIBLE : View.GONE);
                    backButtonText.setText(category.getName().toUpperCase());
                } else {
                    backButtonView.setVisibility(showBackButton ? View.VISIBLE : View.GONE);
                    backButtonText.setText(getString(R.string.fragment_back_to) + " " + category.getName());
                }
            }
        }
    }

    @Click({R.id.fragment_back_view, R.id.fragment_back_button})
    public void onBackButtonClick(View v){
        if(!isTablet || (backButton != null && backButton.getVisibility() == View.VISIBLE))
            getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    @UiThread
    public void showProgressView(){
        if(!successView.isRefreshing()) {
            successView.setVisibility(View.GONE);
            if(emptyView != null)
                emptyView.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
            progressView.setVisibility(View.VISIBLE);
        }
    }

    @UiThread
    public void showErrorView(){
        if(!successView.isRefreshing()) {
            successView.setVisibility(View.GONE);
            if(emptyView != null)
                emptyView.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            progressView.setVisibility(View.GONE);
        }else{
            successView.setRefreshing(false);
        }
    }

    @UiThread
    public void showEmptyView(){
        successView .setVisibility(View.GONE);
        if(emptyView != null)
            emptyView   .setVisibility(View.VISIBLE);
        errorView   .setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);
        if(successView.isRefreshing()) {
            successView.setRefreshing(false);
        }
    }

    @UiThread
    public void showSuccessView(){
        if(!successView.isRefreshing()) {
            successView.setVisibility(View.VISIBLE);
            if(emptyView != null)
                emptyView.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
            progressView.setVisibility(View.GONE);
        }else{
            successView.setRefreshing(false);
        }
    }

    @Click(R.id.empty_retry_button)
    public void onEmptyRetryClick(View v){
        onServerRequest();
    }

    @Click(R.id.error_retry_button)
    public void onErrorRetryClick(View v){
        onServerRequest();
    }

    @UiThread
    public abstract void configuraRecyclerView();

    @Background
    public abstract void onServerRequest();

}
