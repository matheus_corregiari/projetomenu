package br.com.projetox.projetomenu.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;

import br.com.projetox.projetomenu.BuildConfig;
import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.ui.activity.base.ActivityBase;

/**
 * Created by matheus on 8/4/15.
 */
public class Utils {
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public static boolean verifyStoragePermissions(ActivityBase activity) {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        }
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            return true;
        }
        return false;
    }

    public static void getImage(final Context context, final View progress, final ImageView imageView, String url) {
        if(url == null || url.equals("")){
            progress.setVisibility(View.INVISIBLE);
            imageView.setVisibility(View.VISIBLE);
            return;
        }else{
            progress.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.INVISIBLE);
        }
        url = url.replace("/original/", "/medium/");

        final String finalUrl = url;
        final String pathToAlbum = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + context.getString(R.string.app_name);
        final String pathToImage = pathToAlbum + "/" + finalUrl.replace('/', '_').substring(1, finalUrl.indexOf("?") > 0? finalUrl.indexOf("?") : finalUrl.length() - 1);
        final File file = new File(pathToImage);
        Picasso.with(context).load(BuildConfig.IMG_URL + finalUrl).error(R.drawable.ic_no_image_large).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                imageView.setVisibility(View.VISIBLE);
                progress.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError() {
                imageView.setVisibility(View.VISIBLE);
                progress.setVisibility(View.INVISIBLE);
                if(file.exists()) {
                    Picasso.with(context).load(file).into(imageView);
                }else{
                    Log.wtf("FILE", "Não Existe");
                }
            }
        });
        Picasso.with(context).load(BuildConfig.IMG_URL + url).into(new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                if(bitmap == null){
                    return;
                }

                new Thread(new Runnable() {
                    @SuppressWarnings("ResultOfMethodCallIgnored")
                    @Override
                    public void run() {
                        File file = new File(pathToAlbum);
                        if (!file.mkdirs()) {
                            Crashlytics.log("Erro para criar Album!");
                        }
                        File image = new File(pathToImage);
                        FileOutputStream ostream = null;
                        try {
                            file.createNewFile();
                            ostream = new FileOutputStream(image);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                            ostream.close();
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                            Log.wtf("FILE", e.getMessage());
                        }finally {
                            if(ostream != null){
                                try {
                                    ostream.close();
                                } catch (Exception e) {
                                    Crashlytics.logException(e);
                                }
                            }
                        }
                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

        }
    public static SpannableString getPriceLabel(Context context, Double priceBefore, Double priceAfter) {
        String stringAfter  = priceAfter  == null || priceAfter  <= 0? "" : String.format("R$ %1$,.2f", priceAfter);
        String stringBefore = priceBefore == null || priceBefore <= 0? "" : String.format("R$ %1$,.2f", priceBefore);
        String price = "";
        boolean hasStrikeAndBoolean = false;
        if(!stringBefore.isEmpty()){
            price+=stringBefore;
        }
        if(!stringAfter.isEmpty()){
            if(!price.isEmpty()){
                price+=" " + context.getString(R.string.pricae_label_divider) + " ";
                hasStrikeAndBoolean = true;
            }
            price+=stringAfter;
        }
        SpannableString spannableString = new SpannableString(price);
        if(hasStrikeAndBoolean){
            spannableString.setSpan(new StrikethroughSpan(), 0, stringBefore.length(), 0);
            spannableString.setSpan(new StyleSpan(Typeface.BOLD), price.length() - stringAfter.length(), price.length(), 0);
        }
        return spannableString;
    }
}
