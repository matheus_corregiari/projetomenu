package br.com.projetox.projetomenu.ui.activity.base;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import com.crashlytics.android.Crashlytics;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.BooleanRes;

import br.com.projetox.projetomenu.R;
import io.fabric.sdk.android.Fabric;

@EActivity
public class ActivityBase extends AppCompatActivity {

    @BooleanRes(R.bool.isTablet)
    public boolean isTablet;

    @ViewById(R.id.app_bar)
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
    }

    @AfterViews
    public void setupToobar(){
        if(!isTablet){
            if(toolbar != null)
                setSupportActionBar(this.toolbar);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @OptionsItem(android.R.id.home)
    @UiThread
    public void onHomeButtonClick(){
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void setContentView(int layoutResID) {
        if(getResources().getBoolean(R.bool.isTablet))
            supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.setContentView(layoutResID);
    }

}
