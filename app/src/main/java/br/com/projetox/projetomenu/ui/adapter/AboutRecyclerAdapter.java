package br.com.projetox.projetomenu.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Application;
import br.com.projetox.projetomenu.utils.Utils;

/**
 * Created by matheus on 7/21/15.
 */
public class AboutRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String title;
    private boolean hasTitle;
    private Context mContext;
    private Application application;

    public AboutRecyclerAdapter(Context context, Application application, boolean hasTitle, String title) {
        this.mContext    = context;
        this.application = application;
        this.hasTitle    = hasTitle;
        this.title       = title;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch(viewType){
            case 0:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_about_title_list_item, parent, false);
                return new TitleViewHolder(itemView);
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_about_image_item, parent, false);
                return new ImageAboutViewHolder(itemView);
            case 2:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_about_description_item, parent, false);
                return new DescriptionAboutViewHolder(itemView);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return hasTitle? position : position + 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch(getItemViewType(position)){
            case 0:
                setupTitle((TitleViewHolder) viewHolder);
                break;
            case 1:
                setupImageAbout((ImageAboutViewHolder) viewHolder);
                break;
            case 2:
                setupDescriptionAbout((DescriptionAboutViewHolder) viewHolder);
                break;
        }
    }

    private void setupImageAbout(final ImageAboutViewHolder viewHolder) {
        String imgUrl = application.getPhotos() != null && !application.getPhotos().isEmpty()? application.getPhotos().get(0).getImgUrl() : "";
        Utils.getImage(mContext, viewHolder.imageProgress, viewHolder.imageView, imgUrl);
    }

    private void setupDescriptionAbout(final DescriptionAboutViewHolder viewHolder) {
        viewHolder.descriptionText.setText(application.getDescription());
    }

    private void setupTitle(final TitleViewHolder viewHolder) {
        viewHolder.nameText.setText(title);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return hasTitle? 3 : 2;
    }

    public class ImageAboutViewHolder extends RecyclerView.ViewHolder {
        public View      layoutView;
        public ImageView imageView;
        public View      imageProgress;

        public ImageAboutViewHolder(View itemView) {
            super(itemView);
            this.layoutView    =  itemView.findViewById(R.id.fragment_about_layout_view);
            this.imageView     = (ImageView) itemView.findViewById(R.id.fragment_about_image_view);
            this.imageProgress =  itemView.findViewById(R.id.fragment_about_image_progress);
        }
    }

    public class DescriptionAboutViewHolder extends RecyclerView.ViewHolder {
        public View     layoutView;
        public TextView descriptionText;

        public DescriptionAboutViewHolder(View itemView) {
            super(itemView);
            this.layoutView      =  itemView.findViewById(R.id.fragment_about_description_layout_view);
            this.descriptionText = (TextView)  itemView.findViewById(R.id.fragment_about_description_text);
        }
    }

    public class TitleViewHolder extends RecyclerView.ViewHolder {
        public View      layoutView;
        public TextView  nameText;

        public TitleViewHolder(View itemView) {
            super(itemView);
            this.layoutView = itemView.findViewById(R.id.fragment_about_title_layout);
            this.nameText   = (TextView)  itemView.findViewById(R.id.fragment_about_title_text);
        }
    }
}