package br.com.projetox.projetomenu.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.ui.activity.base.ActivityBase;
import br.com.projetox.projetomenu.utils.Utils;

public class ActivitySplash extends ActivityBase {

    private static final long TIME_MIN = 1500;
    private boolean started = false;
    private Handler handler;
    private Runnable runnable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_splash);
        if(!getResources().getBoolean(R.bool.isTablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!Utils.verifyStoragePermissions(this)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final long startTime = System.nanoTime();
                    long time = System.nanoTime() - startTime;
                    long wait = TIME_MIN - (time) / 1000000;
                    startActivity(wait);
                }
            }).start();
        }else{

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(handler != null && runnable != null){
            handler.removeCallbacks(runnable);
        }
    }

    private void startActivity(final long wait){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(!started) {
                                    started = true;
                                    Intent intent = new Intent(getApplicationContext(), ActivityMain_.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                    }
                };
                handler = new Handler();
                handler.postDelayed(runnable, wait);
            }
        });
    }
}
