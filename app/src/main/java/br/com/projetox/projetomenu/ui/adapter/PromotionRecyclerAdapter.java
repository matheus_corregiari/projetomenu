package br.com.projetox.projetomenu.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Promotion;
import br.com.projetox.projetomenu.ui.activity.ActivityPromotionDetail_;
import br.com.projetox.projetomenu.utils.Utils;

/**
 * Created by matheus on 7/21/15.
 */
public class PromotionRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Promotion> promotionList;

    public PromotionRecyclerAdapter(Context context, List<Promotion> productList) {
        this.mContext = context;
        this.promotionList = productList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch(viewType){
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_promotion_list_item, parent, false);
                return new PromotionViewHolder(itemView);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if(getItemViewType(position) == 1) {
            setupPromotion((PromotionViewHolder) viewHolder, position);
        }
    }

    private void setupPromotion(final PromotionViewHolder viewHolder, int position) {
        final Promotion promotion = promotionList.get(position);
        viewHolder.nameText.setText(promotion.getName());
        viewHolder.priceText.setText(Utils.getPriceLabel(mContext, promotion.getPriceBefore(), promotion.getPriceAfter()));
        Utils.getImage(mContext, viewHolder.imageProgress, viewHolder.imageView, promotion.getImgUrl());
        viewHolder.layoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityPromotionDetail_.class);
                intent.putExtra("PROMOTION_DETAIL", promotion);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return promotionList.size();
    }

    public class PromotionViewHolder extends RecyclerView.ViewHolder {
        public View             layoutView;
        public RoundedImageView imageView;
        public View             imageProgress;
        public TextView         nameText;
        public TextView         priceText;

        public PromotionViewHolder(View itemView) {
            super(itemView);
            this.layoutView    =  itemView.findViewById(R.id.fragment_promotion_list_layout_view);
            this.imageView     = (RoundedImageView) itemView.findViewById(R.id.fragment_promotion_list_image_view);
            this.imageProgress =  itemView.findViewById(R.id.fragment_promotion_list_image_progress);
            this.nameText      = (TextView)  itemView.findViewById(R.id.fragment_promotion_list_name_text);
            this.priceText     = (TextView)  itemView.findViewById(R.id.fragment_promotion_list_price_text);
        }
    }

}