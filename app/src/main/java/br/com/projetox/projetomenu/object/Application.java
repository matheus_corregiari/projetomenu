package br.com.projetox.projetomenu.object;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matheus on 7/21/15.
 */
public class Application implements Serializable{
    @Expose
    private Long id;
    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private List<Address> addresses = new ArrayList<Address>();
    @Expose
    private List<Phone> phones = new ArrayList<Phone>();
    @Expose
    private List<Album> albums = new ArrayList<Album>();
    @Expose
    private List<Photo> photos = new ArrayList<Photo>();

    public Application() {
        this.id          = 0l;
        this.name        = "";
        this.description = "";
        this.addresses   = new ArrayList<>();
        this.phones      = new ArrayList<>();
        this.albums      = new ArrayList<>();
        this.photos      = new ArrayList<>();
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The addresses
     */
    public List<Address> getAddresses() {
        return addresses;
    }

    /**
     *
     * @param addresses
     * The addresses
     */
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    /**
     *
     * @return
     * The phones
     */
    public List<Phone> getPhones() {
        return phones;
    }

    /**
     *
     * @param phones
     * The phones
     */
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    /**
     *
     * @return
     * The albums
     */
    public List<Album> getAlbums() {
        return albums;
    }

    /**
     *
     * @param albums
     * The albums
     */
    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    /**
     *
     * @return
     * The photos
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     *
     * @param photos
     * The photos
     */
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

}