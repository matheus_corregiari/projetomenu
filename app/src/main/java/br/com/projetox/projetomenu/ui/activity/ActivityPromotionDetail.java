package br.com.projetox.projetomenu.ui.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.object.Promotion;
import br.com.projetox.projetomenu.ui.activity.base.ActivityBase;
import br.com.projetox.projetomenu.utils.Utils;

@EActivity(R.layout.acivity_promotion_detail)
public class ActivityPromotionDetail extends ActivityBase {

    @Extra("PROMOTION_DETAIL")
    @InstanceState
    public Promotion promotion;

    @ViewById(R.id.activity_promotion_detail_image_view)
    public ImageView promotionImage;

    @ViewById(R.id.activity_promotion_detail_image_progress)
    public View promotionImageProgress;

    @ViewById(R.id.activity_promotion_detail_name_text)
    public TextView promotionName;

    @ViewById(R.id.activity_promotion_detail_price_text)
    public TextView promotionPrice;

    @ViewById(R.id.activity_promotion_detail_description_text)
    public TextView promotionDescription;

    @ViewById(R.id.activity_promotion_detail_description_view)
    public View promotionDescriptionView;

    @Override
    @SuppressWarnings("ConstantConditions")
    @AfterViews
    public void setupToobar(){
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @AfterViews
    public void updateUI(){
        promotionName.setText(promotion.getName().toUpperCase());
        promotionPrice.setText(Utils.getPriceLabel(this, promotion.getPriceBefore(), promotion.getPriceAfter()));
        promotionDescription.setText(promotion.getDescription());
        promotionDescriptionView.setVisibility(promotion.getDescription() == null || promotion.getDescription().isEmpty() ? View.GONE : View.VISIBLE);
        Utils.getImage(this, promotionImageProgress, promotionImage, promotion.getImgUrl());
    }
}
