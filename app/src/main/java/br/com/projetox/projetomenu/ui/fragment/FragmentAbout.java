package br.com.projetox.projetomenu.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.projetox.projetomenu.BuildConfig;
import br.com.projetox.projetomenu.R;
import br.com.projetox.projetomenu.api.API;
import br.com.projetox.projetomenu.object.Application;
import br.com.projetox.projetomenu.object.Category;
import br.com.projetox.projetomenu.object.Promotion;
import br.com.projetox.projetomenu.ui.activity.ActivityMain;
import br.com.projetox.projetomenu.ui.adapter.AboutRecyclerAdapter;
import br.com.projetox.projetomenu.ui.fragment.base.FragmentBase;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ActivityMain}
 * on handsets.
 */
@EFragment(R.layout.fragment_about)
public class FragmentAbout extends FragmentBase {

    @ViewById(R.id.fragment_about_recycler_view)
    public RecyclerView recyclerView;

    public Application application;

    public FragmentAbout() {
    }

    public static Fragment newInstance(Category category){
        Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_KEY_ARGUMENT_CATEGORY_ID, category);
        FragmentAbout_ fragment = new FragmentAbout_();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    @AfterViews
    @Background
    public void onServerRequest(){
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(BuildConfig.HOST).build();
        API api = restAdapter.create(API.class);
        showProgressView();
        String key = "FragmentAbout:"+BuildConfig.REQUEST_APPLICATION_ID;
        application = getLocal(key);
        if (application != null) {
            configuraRecyclerView();
            showSuccessView();
        }
        final String finalKey = key;
        api.getApplication(BuildConfig.REQUEST_APPLICATION_ID, new Callback<Application>() {
            @Override
            public void success(Application application, Response response) {
                if (response.getStatus() == 200 && application != null) {
                    FragmentAbout.this.application = application;
                    saveLocal(finalKey, application);
                } else {
                    if (application == null) {
                        showErrorView();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (application == null) {
                    showErrorView();
                }
            }
        });
    }

    @Background
    public void saveLocal(String key, Application application){
        if(key == null || key.isEmpty()){
            return;
        }
        if(application == null){
            return;
        }
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "APPLICATION");
            snappydb.put(key, application);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        configuraRecyclerView();
        showSuccessView();
    }

    public Application getLocal(String key){
        if(key == null || key.isEmpty()){
            return null;
        }
        Application application = null;
        DB snappydb = null;
        try {
            snappydb = DBFactory.open(getActivity(), "APPLICATION");
            application = snappydb.getObject(key, Application.class);
        } catch (SnappydbException e) {
            Crashlytics.logException(e);
        }finally {
            try {
                if(snappydb != null && snappydb.isOpen())
                    snappydb.close();
            } catch (SnappydbException e) {
                Crashlytics.logException(e);
            }
        }
        return application;
    }

    @Override
    @UiThread
    public void configuraRecyclerView(){
        final AboutRecyclerAdapter adapter = new AboutRecyclerAdapter(getActivity(), application, isTablet, category.getName().toUpperCase());
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.scrollToPosition(0);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
