package br.com.projetox.projetomenu.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by matheus on 7/21/15.
 */
public class Photo implements Serializable{

    @Expose
    private Long id;
    @SerializedName("application_id")
    @Expose
    private Long applicationId;
    @SerializedName("order_by")
    @Expose
    private Long orderBy;
    @Expose
    private Boolean active;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("album_id")
    @Expose
    private Long albumId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Photo() {
        this.id = 0l;
        this.applicationId = 0l;
        this.orderBy = 0l;
        this.active = true;
        this.imgUrl = "";
        this.albumId = 0l;
        this.createdAt = "";
        this.updatedAt = "";
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The applicationId
     */
    public Long getApplicationId() {
        return applicationId;
    }

    /**
     *
     * @param applicationId
     * The application_id
     */
    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    /**
     *
     * @return
     * The orderBy
     */
    public Long getOrderBy() {
        return orderBy;
    }

    /**
     *
     * @param orderBy
     * The order_by
     */
    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    /**
     *
     * @return
     * The active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * @param active
     * The active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     *
     * @return
     * The imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     *
     * @param imgUrl
     * The img_url
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     *
     * @return
     * The albumId
     */
    public Long getAlbumId() {
        return albumId;
    }

    /**
     *
     * @param albumId
     * The album_id
     */
    public void setAlbumId(Long albumId) {
        this.albumId = albumId;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}